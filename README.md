#  Deep neural networks concepts

Outline and content for a half-day workshop covering the core concepts behind deep neural networks.

You are welcome to re-use all the materials here. I'd appreciate it if you tell people where you got them.

## Presentation plan

1. [How neural networks work](https://docs.google.com/presentation/d/1AAEFCgC0Ja7QEl3-wmuvIizbvaE-aQRksc7-W8LR2GY/edit?usp=sharing)
2. How convolutional neural networks work
3. [How recurrent neural networks work (including LSTMs)](https://docs.google.com/presentation/d/1hqYB3LRwg_-ntptHxH18W1ax9kBwkaZ1Pa_s3L7R-2Y/edit?usp=sharing)
4. [How optimization works](https://docs.google.com/presentation/d/1zKufm-OsGn6UVxiRq2gvlpN22mang-bW-YwZScuPWTo/edit?usp=sharing)
5. [How backpropagation works](https://docs.google.com/presentation/d/1DvzP2Q-VfyNj9RwrjVOCOyZSE0PPPZUj5AxEQDouav4/edit?usp=sharing)
6. [What neural networks can learn](https://docs.google.com/presentation/d/1DvzP2Q-VfyNj9RwrjVOCOyZSE0PPPZUj5AxEQDouav4/edit?usp=sharing)